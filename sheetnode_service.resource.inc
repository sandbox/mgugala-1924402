<?php 
/**
 * Callback for sreating sheetnode resources.
 *
 * @param $node
 *   Node array with data to put in sheetnode node
 *
 * @return object
 *   Node object
 */
function _sheetnode_service_resource_create($node) {
  // Adds backwards compatability with regression fixed in #1083242
  $node = _services_arg_value($node, 'node');
  
  if (!isset($node['name'])) {
    // Assign username to the node from $user created at auth step.
    global $user;
    $node['name'] = $user->name;
  }
  
  // Get sheet content
  $sheetcontent = base64_decode($node['sheetnode']['value']);
  // Save to the tmp file
  $filename = 'sheetnode-sheet';
  $filepath = file_unmanaged_save_data($sheetcontent, file_directory_temp() . '/' . $filename, FILE_EXISTS_RENAME);
  // Unset encoded value of the file
  unset($node['sheetnode']['value']);
  $node['sheetnode']['value'] = _sheetnode_excel2socialcalc($filepath);
  
  // Validate the node. If there is validation error Exception will be thrown
  // so code below won't be executed.
  _node_resource_validate_type($node);

  // Load the required includes for drupal_execute
  module_load_include('inc', 'node', 'node.pages');

  $node_type = $node['type'];
  
  if ($node_type != 'sheetnode') {
    return services_error('Content type have to be: sheetnode', 406);
  }

  // Setup form_state
  $form_state = array();
  $form_state['values'] = $node;
  $form_state['values']['op'] = variable_get('services_node_save_button_' . $node_type . '_resource_create', t('Save'));

  drupal_form_submit($node_type . '_node_form', $form_state, (object)$node);

  if ($errors = form_get_errors()) {
    return services_error(implode(" ", $errors), 406, array('form_errors' => $errors));
  }
  // Fetch $nid out of $form_state
  $nid = $form_state['nid'];
  // Only add the URI for servers that support it.
  $node = array('nid' => $nid);
  if ($uri = services_resource_uri(array('node', $nid))) {
    $node['uri'] = $uri;
  }
  return $node;
}
/**
 * Converts content of given file to the socialcalc string
 * 
 * @param  $filepath full path to the file
 * @return socialcalc string
 */
function _sheetnode_excel2socialcalc($filepath) {
  if (file_exists($filepath) && is_readable($filepath)) {   
    // Conversion excel -> socialcalc
    module_load_include('inc', 'sheetnode_phpexcel', 'sheetnode_phpexcel.import');
    $workbook = PHPExcel_IOFactory::load($filepath);
    $socialcalc = _sheetnode_phpexcel_import_do($workbook, $workbook->getSheet(0));
    // Delete temp file
    drupal_unlink($filepath);
    return $socialcalc;
  }
  return '';
}